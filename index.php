<!doctype html>
<html lang="ru">
<head>
    <style>
        a {
            display: inline-block;
            margin-right: 5px;
            text-decoration: none;
        }
        a:hover {
            color: red;
            text-decoration: underline;
        }
        li {
            height: 30px;
        }
    </style>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<a href="create_file.php">Создать файл</a>
<a href="upload_file.php">Загрузить файл</a><br>
<?php

error_reporting(-1);

$dir = opendir('files');
echo 'Список файлов:<ul>';
while ($file_name = readdir($dir)){
    if (is_dir($file_name)) continue;
    echo <<<TAG
    <li><a href="read_file.php?file_name=$file_name">$file_name</a>
    <a href="edit_file.php?file_name=$file_name">Редактировать</a>
    <a href="download_file.php?file_name=$file_name">Скачать</a>
    <a href="delete_file.php?file_name=$file_name">Удалить</a></li>
TAG;
}
echo '</ul>';
?>
</body>
</html>
