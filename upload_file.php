<?php

error_reporting(-1);

$files = scandir('./files');

if(isset($_POST['upload']) AND isset($_FILES['file'])) {
    $file_name = $_FILES['file']['name'];
    if(!in_array($file_name, $files)) {
        $type = $_FILES['file']['type'];
        $size = $_FILES['file']['size'];
        if($size <= 2097152 AND $type == 'text/plain') {
            $file_path = './files/'.$file_name;
            if(move_uploaded_file($_FILES['file']['tmp_name'], $file_path)) {
                echo "<span class=\"success\">Файл загружен!</span><br>";
            } else echo "<span class=\"error\">Возникла ошибка при загрузке файла!</span><br>";
        } else echo "<span class=\"error\">Файл должен быть текстового типа и не больше 2МБ</span><br>";
    } else echo "<span class=\"error\">Файл с таким именем уже существует</span><br>";
}
?>
<!doctype html>
<html lang="ru">
<head>
    <style>
        .success {
            color: green;
        }
        .error {
            color: red;
        }
    </style>
    <meta charset="UTF-8">
    <title>Upload File</title>
</head>
<body>
<form action="upload_file.php" method="post" enctype="multipart/form-data">
    <label>Выберите файл для загрузки: <input type="file" name="file"/></label><br/>
    <input type="submit" value="Загрузить файл" name="upload"/>
</form>
<a href="index.php">На главную</a>
</body>
</html>