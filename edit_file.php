<!doctype html>
<html lang="ru">
<head>
    <style>
        .success {
            color: green;
        }
    </style>
    <meta charset="UTF-8">
    <title>Edit File</title>
</head>
<body>
<?php
error_reporting(-1);

$file = $_GET['file_name'];
$file_path = './files/'.$file;

function get_file_text($file_path) {
    $handler = fopen($file_path, 'r');

    ob_start();

    fpassthru($handler);

    $file_content = ob_get_clean();

    fclose($handler);

    echo $file_content;
}

if(isset($_POST['change'])) {
    $handler = fopen($file_path, 'w+');

    $file_content = $_POST['text'];

    fwrite($handler, $file_content);

    fclose($handler);
    echo "<span class=\"success\">Файл успешно отредактирован!</span><br>";
}
?>
<form action="edit_file.php?file_name=<?php echo $file; ?>" method="post">
    <label>Содержимое файла:<br>
    <textarea name="text" cols="60" rows="30"><?php get_file_text($file_path) ?></textarea></label><br>
    <input type="submit" name="change" value="Применить изменения"/>
</form>
<a href="index.php">На главную</a>
</body>
</html>