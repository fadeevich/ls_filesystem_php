<?php

error_reporting(-1);

$files = scandir('./files');

if(isset($_POST['create'])) {
    $file_name = $_POST['file_name'].'.txt';
    if(!in_array($file_name, $files)) {
        $text = $_POST['text'];
        $file_path = './files/'.$file_name;
        $handler = fopen($file_path, 'a');
        fwrite($handler, $text);
        fclose($handler);
        echo "<span class=\"success\">Файл создан!</span><br>";
    } else echo "<span class=\"error\">Файл с таким именем уже существует, измените имя или удалите файл, который хотите заменить</span><br>";
}

?>
<!doctype html>
<html lang="ru">
<head>
    <style>
        .success {
            color: green;
        }
        .error {
            color: red;
        }
    </style>
    <meta charset="UTF-8">
    <title>Create File</title>
</head>
<body>
<form action="create_file.php" method="post">
    <label>Введите имя файла: <input type="text" name="file_name"/>.txt</label><br>
    <label>Введите содержимое файла:<br/> <textarea name="text" cols="40" rows="20"></textarea></label><br>
    <input type="submit" name="create" value="Создать файл"/><br>
</form>
<a href="index.php">На главную</a>
</body>
</html>